%define debug_package %{nil}
%define repo github.com/syncthing/syncthing
Name:           syncthing
Version:        1.28.0
Release:        2%{?dist}
Summary:        Open, trustworthy and decentralized sync

License:        MPLv2.0
URL:            https://%{repo}
Source0:        https://%{repo}/releases/download/v%{version}/%{name}-source-v%{version}.tar.gz

BuildRequires:  git golang systemd npm

AutoReq:        no
AutoReqProv:    no

%description
Syncthing replaces proprietary sync and cloud services with something open,
trustworthy and decentralized. Your data is your data alone and you deserve
to choose where it is stored, if it is shared with some third party and how
it's transmitted over the Internet.

%package        tools
Summary:        Tools from the Syncthing project 

%description    tools
The tools that are created by the Syncthing project.

%prep
%setup -q -c -n %{name}
mkdir -p $(dirname src/%{repo})
mv %{name} src/%{repo}

%build
export GOPATH="$(pwd)"
export PATH=$PATH:"$(pwd)"/bin
cd src/%{repo}
go run build.go -no-upgrade -version "v%{version}" install all

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_userunitdir}

cp src/%{repo}/bin/* %{buildroot}%{_bindir}
cp src/%{repo}/etc/linux-systemd/system/syncthing\@.service  %{buildroot}%{_unitdir}
cp src/%{repo}/etc/linux-systemd/user/syncthing.service %{buildroot}%{_userunitdir}


%files
%{_bindir}/syncthing
%{_unitdir}/syncthing@.service
%{_userunitdir}/syncthing.service
%license src/%{repo}/LICENSE

%files tools
%{_bindir}/stdiscosrv
%{_bindir}/strelaysrv

%changelog
* Mon Nov 25 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.28.0-1
- Update to v1.28.0
- Update to v1.27.8

* Sun Mar 10 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.27.4-2
- Fix version string

* Sat Mar 9 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.27.4-1
- Update to v1.27.4

* Fri Aug 11 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.7-1
- Update to v1.23.6

* Thu Jul 13 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.6-1
- Update to v1.23.6

* Tue Jun 6 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.5-1
- Update to v1.23.5

* Sat Apr 8 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.3-1
- Update to v1.23.3

* Fri Mar 10 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.2-1
- Update to v1.23.2

* Mon Jan 2 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.23.0-1
- Update to v1.23.0

* Tue Nov 29 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.22.2-1
- Update to v1.22.2

* Sat Nov 5 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.22.1-1
- Update to v1.22.1

* Tue Oct 4 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.22.0-1
- Update to v1.22.0

* Thu Sep 15 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.21.0-1
- Update to v1.21.0

* Tue May 3 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.20.0-1
- Update to v1.20.0

* Sun Mar 6 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.19.1-1
- Update to v1.19.1

* Tue Feb 1 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.19.0-1
- Update to v1.19.0

* Mon Jul 26 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.18.0-1
- Update to v1.18.0

* Fri Jun 04 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.17.0-1
- Update to v1.17.0

* Tue May 04 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.16.0-1
- Update to v1.16.0
- Build the new interface
- Add npm dependency
- Differentiate el7 build
- Remove stindex as it is now a subcommand

* Mon May 03 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.15.0-1
- Update to v1.15.0
- Remove stcli as it is now a subcommand

* Wed Jan 27 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.12.1-1
- Update to v1.12.1

* Thu Nov 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.11.1-1
- Update to v1.11.1

* Sat Sep 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.9.0-1
- Update to v1.9.0

* Wed Aug 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.8.0-1
- Update to v1.8.0

* Fri Jul 24 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.7.1-1
- Update to v1.7.1

* Tue Jul 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.7.0-1
- Update to v1.7.0

* Tue Jun 09 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.6.1-1
- Update to v1.6.1

* Tue Jun 09 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.6.0-1
- Update to v1.6.0

* Tue May 05 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.5.0-1
- Update to v1.5.0

* Wed Apr 08 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.4.2-1
- Update to v1.4.2

* Mon Apr 06 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.4.1-1
- Update to v1.4.1

* Mon Mar 16 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.4.0-1
- Update to v1.4.0

* Tue Feb 04 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.4-2
- Update to v1.3.4

* Tue Jan 07 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.3-2
- Update to v1.3.3

* Wed Oct 02 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.0-2
- Update to v1.3.0

* Mon Sep 02 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.2.2-2
- Update to v1.2.2

* Mon Aug 12 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.2.1-2
- Update to v1.2.1

* Fri Jul 19 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.2.0-2
- Create tools package

* Tue Jul 09 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.2.0-1
- Update to v1.2.0

* Mon Jun 17 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.1.4-1
- Update to v1.1.4

* Thu May 09 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.1.3-1
- Update to v1.1.3

* Mon May 06 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.1.2-1
- Update to v1.1.2

* Mon Apr 08 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.1.1-1
- Update to v1.1.1

* Thu Feb 07 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.1-1
- Update to v1.0.1

* Thu Jan 03 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.0.0-1
- Update to v1.0.0

* Wed Oct 03 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.51-1
- Update to v0.14.51

* Thu Sep 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.50-1
- Update to v0.14.50

* Wed Jul 25 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.49-1
- Update to v0.14.49

* Tue Jun 05 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.48-1
- Update to v0.14.48

* Tue May 01 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.47-1
- Update to v0.14.47

* Thu Apr 05 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.46-1
- Improve build process
- Add license file

* Thu Apr 05 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.46
- Update to v0.14.46

* Wed Apr 04 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.45
- Update to v0.14.45

* Sat Jan 13 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.43
- Update to v0.14.43

* Wed Dec 27 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.42
- Update to v0.14.42

* Wed Dec 06 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.41
- Update to v0.14.41

* Wed Nov 15 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.40
- Update to v0.14.40

* Thu Oct 12 2017 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.14.39
- Update to v0.14.39

* Wed Sep 20 2017 daftaupe <daftaupe@protonmail.com> 0.14.38
- Update to v0.14.38

* Thu Sep 07 2017 daftaupe <daftaupe@protonmail.com> 0.14.37
- Update to v0.14.37

* Fri Aug 25 2017 daftaupe <daftaupe@protonmail.com> 0.14.33
- Update to v0.14.33

* Tue Jul 25 2017 daftaupe <daftaupe@protonmail.com> 0.14.32
- Update to v0.14.32

* Tue Jun 27 2017 daftaupe <daftaupe@protonmail.com> 0.14.31
- Update to v0.14.31

* Tue Jun 13 2017 daftaupe <daftaupe@protonmail.com> 0.14.30
- Update to v0.14.30

* Mon Jun 12 2017 daftaupe <daftaupe@protonmail.com> 0.14.29
- Update to v0.14.29

* Mon Jun 12 2017 daftaupe <daftaupe@protonmail.com> 0.14.28
- Update to v0.14.28

* Mon Jun 12 2017 daftaupe <daftaupe@protonmail.com> 0.14.27
- Update to v0.14.27

* Mon Jun 12 2017 daftaupe <daftaupe@protonmail.com> 0.14.26
- Bump syncthing version 0.14.25 -> 0.14.26

* Sun Mar 26 2017 daftaupe <daftaupe@protonmail.com> 0.14.25
- Bump syncthing version 0.14.24 -> 0.14.25

* Sun Mar 26 2017 daftaupe <daftaupe@protonmail.com> 0.14.24
- Bump syncthing version 0.14.23 -> 0.14.24

* Fri Feb 10 2017 daftaupe <daftaupe@protonmail.com> 0.14.23
- Adapted from Javier Wilson spec file to build from source tarball

* Thu Feb  9 2017 daftaupe <daftaupe@protonmail.com> 0.14.23
- Bump syncthing version 0.14.7 -> 0.14.23

* Thu Sep 22 2016 Logan Owen <logan@s1network.com> 0.14.7
- Bump syncthing version 0.13.1 -> 0.14.7

* Mon Feb 08 2016 Martin Lazarov <martin@lazarov.bg> 0.13.1
- Initial spec version
